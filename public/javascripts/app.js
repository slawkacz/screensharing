window.onload = function () {
    var socket = io.connect('http://localhost');

    var peer = new Peer({key: 'k7vpr9xlmbc323xr'});
    var connectWrapper = document.querySelector('.connect-wrapper');
    var peerName = connectWrapper.querySelector('.peerName');
    var connectButton = connectWrapper.querySelector('.connectBtn');
    var peersWrapper = document.querySelector('.connected-peers');
    var peersList = peersWrapper.querySelector('.peers-list');
    var shareButton = peersWrapper.querySelector('.shareBtn');
    shareButton.addEventListener('click',function(){
        var peersToConnect = Array.prototype.slice.call(peersList.querySelectorAll('input[name="peers[]"]:checked'));
        peersToConnect.forEach(function(el){
            var conn = peer.connect(el.value);
            conn.on('open', function(){

                conn.send('hi!');
            });
        });
    },false);
    peer.on('connection', function(conn) {
        conn.on('data', function(data){
            // Will print 'hi!'
            console.log(data);
        });
    });
    connectButton.disabled = false;
    var peerId;
    var connected;
    peer.on('open', function (id) {
        connectWrapper.hidden = false;
        peerId = id;
        console.log('My peer ID is: ' + id);
    });

    socket.on('getPeers', function (data) {
        if (connected) {
            peersList.innerHTML = '';
            data.forEach(function (el) {
                var li = document.createElement('li'),
                    input = document.createElement('input');
                input.type = 'checkbox';
                input.name = 'peers[]';
                input.value = el.peerId;
                li.appendChild(input);
                li.appendChild(document.createTextNode(el.name));
                peersList.appendChild(li);
                console.log(el)
            });
        }
//
        // socket.emit('my other event', { my: 'data' });
    });
    socket.on('connected', function (data) {
        connectWrapper.hidden = true;
        connected = true;
        console.log(data);
        socket.emit('getPeers');
    });
    connectButton.addEventListener('click', function (e) {
        e.preventDefault();
        var target = e.target;
        if (peerName.value) {
            target.disabled = true;
            socket.emit('addPeer', {peerId: peerId, name: peerName.value});
        }
    }, false);
};