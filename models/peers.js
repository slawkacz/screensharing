var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PeersSchema = new Schema({
    name: String,
    peerId: String,
    timestamp: Number

});

module.exports = mongoose.model('Peers', PeersSchema);