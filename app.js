/**
 * Module dependencies.
 */

var express = require('express')
    , routes = require('./routes'),
    mongoose = require('mongoose'),
    config = require('./config/config')
    ;

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});
;

var app = module.exports = express.createServer();

// Configuration

app.configure(function () {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function () {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function () {
    app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);

app.listen(3000, function () {
    console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});

var io = require('socket.io').listen(app);
var PeersModel = require('./models/peers');
io.sockets.on('connection', function (socket) {
    socket.on('addPeer', function (data) {
        var query = PeersModel
            .find()
            .where('name')
            .equals(data.name);
        query.exec(function (err, peers) {
            if (err) {
                console.error(err);
                return;
            }
            if (peers.length) {
                peers.forEach(function (peer) {
                    peer.remove();
                });
            }

        });
        var d = new PeersModel({
            name: data.name,
            peerId: data.peerId,
            timestamp: Date.now()
        });

        d.save(function (err, peer) {
            if (err) {
                console.error(err);
                return;
            }
            socket.emit('getPeers', data);
            socket.emit('connected', {connected: true});
        });
    });
    socket.on('getPeers', function (data) {
        var query = PeersModel
            .find();
        query.exec(function (err, peers) {
            if (err) {
                console.error(err);
                return;
            }
            if (peers.length) {
                socket.broadcast.emit('getPeers', peers);
                socket.emit('getPeers', peers);
            }

        });
    });
});
